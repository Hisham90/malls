<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MallsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MallsTable Test Case
 */
class MallsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MallsTable
     */
    public $Malls;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.malls',
        'app.users',
        'app.shops',
        'app.malls_shops'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Malls') ? [] : ['className' => MallsTable::class];
        $this->Malls = TableRegistry::get('Malls', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Malls);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

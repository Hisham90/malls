<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StateTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StateTable Test Case
 */
class StateTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StateTable
     */
    public $State;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.state',
        'app.malls',
        'app.users',
        'app.shops',
        'app.malls_shops',
        'app.malls_state'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('State') ? [] : ['className' => StateTable::class];
        $this->State = TableRegistry::get('State', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->State);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

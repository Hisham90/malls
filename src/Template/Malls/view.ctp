<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Mall $mall
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Mall'), ['action' => 'edit', $mall->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Mall'), ['action' => 'delete', $mall->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mall->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Malls'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Mall'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Shops'), ['controller' => 'Shops', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Shop'), ['controller' => 'Shops', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="malls view large-9 medium-8 columns content">
    <h3><?= h($mall->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $mall->has('user') ? $this->Html->link($mall->user->name, ['controller' => 'Users', 'action' => 'view', $mall->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($mall->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address') ?></th>
            <td><?= h($mall->address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('State') ?></th>
            <td><?= h($mall->state) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Phone') ?></th>
            <td><?= h($mall->phone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Url') ?></th>
            <td><?= h($mall->url) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($mall->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Public Holiday') ?></th>
            <td><?= h($mall->public_holiday) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($mall->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Bussiness Hours Start') ?></th>
            <td><?= h($mall->bussiness_hours_start) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Busssiness Hours Finish') ?></th>
            <td><?= h($mall->busssiness_hours_finish) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($mall->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($mall->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('About') ?></h4>
        <?= $this->Text->autoParagraph(h($mall->about)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Shops') ?></h4>
        <?php if (!empty($mall->shops)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('About') ?></th>
                <th scope="col"><?= __('Category') ?></th>
                <th scope="col"><?= __('Location') ?></th>
                <th scope="col"><?= __('Contact') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($mall->shops as $shops): ?>
            <tr>
                <td><?= h($shops->id) ?></td>
                <td><?= h($shops->name) ?></td>
                <td><?= h($shops->about) ?></td>
                <td><?= h($shops->category) ?></td>
                <td><?= h($shops->location) ?></td>
                <td><?= h($shops->contact) ?></td>
                <td><?= h($shops->email) ?></td>
                <td><?= h($shops->created) ?></td>
                <td><?= h($shops->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Shops', 'action' => 'view', $shops->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Shops', 'action' => 'edit', $shops->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Shops', 'action' => 'delete', $shops->id], ['confirm' => __('Are you sure you want to delete # {0}?', $shops->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>

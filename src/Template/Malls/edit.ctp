<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Mall $mall
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $mall->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $mall->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Malls'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Shops'), ['controller' => 'Shops', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Shop'), ['controller' => 'Shops', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="malls form large-9 medium-8 columns content">
    <?= $this->Form->create($mall) ?>
    <fieldset>
        <legend><?= __('Edit Mall') ?></legend>
        <?php
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('name');
            echo $this->Form->control('about');
            echo $this->Form->control('address');
            echo $this->Form->control('state');
            echo $this->Form->control('phone');
            echo $this->Form->control('url');
            echo $this->Form->control('email');
            echo $this->Form->control('bussiness_hours_start', ['empty' => true]);
            echo $this->Form->control('busssiness_hours_finish', ['empty' => true]);
            echo $this->Form->control('public_holiday');
            echo $this->Form->control('shops._ids', ['options' => $shops]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

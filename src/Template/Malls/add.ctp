<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Mall $mall
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Malls'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Shops'), ['controller' => 'Shops', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Shop'), ['controller' => 'Shops', 'action' => 'add']) ?></li>
    </ul>
</nav>



<div class="malls form large-9 medium-8 columns content">
    <?= $this->Form->create($mall) ?>
    <fieldset>
        <legend><?= __('Add Mall') ?></legend>
        <?php
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('name',[
                                    'label' => 'Nama'
                                ]);
            echo $this->Form->control('about',[
                                    'label' => 'Details'
                                        ]);
            echo $this->Form->control('address');
            //echo $this->Form->control('state');


            $options = [
    ['text' => '-Please Choose-', 'value' => '-Please Choose-', 'attr_name' => '-Please Choose-'],
    ['text' => 'Sarawak', 'value' => 'Sarawak', 'attr_name' => 'Sarawak'],
    ['text' => 'Sabah', 'value' => 'Sabah', 'attr_name' => 'Sabah'],
    ['text' => 'Pahang', 'value' => 'Pahang', 'attr_name' => 'Pahang'],
    ['text' => 'Perak ', 'value' => 'Perak', 'attr_name' => 'Perak'],
    ['text' => 'Johor', 'value' => 'Johor', 'attr_name' => 'Johor'],
    ['text' => 'Kelantan', 'value' => 'Kelantan', 'attr_name' => 'Kelantan'],
    ['text' => 'Terengganu', 'value' => 'Terengganu', 'attr_name' => 'Terengganu'],
    ['text' => 'Kedah', 'value' => 'Kedah', 'attr_name' => 'Kedah'],
    ['text' => 'Selangor', 'value' => 'Selangor', 'attr_name' => 'Selangor'],
    ['text' => 'Negeri Sembilan', 'value' => 'Negeri Sembilan', 'attr_name' => 'Negeri Sembilan'],
    ['text' => 'Negeri Sembilan', 'value' => 'Negeri Sembilan', 'attr_name' => 'Negeri Sembilan'],
    ['text' => 'Pulau Pinang', 'value' => 'Pulau Pinang', 'attr_name' => 'Pulau Pinang'],
    ['text' => 'Perlis', 'value' => 'Perlis', 'attr_name' => 'Perlis'],
    ['text' => 'Kuala Lumpur', 'value' => 'Kuala Lumpur', 'attr_name' => 'Kuala Lumpur'],
   
    
];
echo $this->Form->select('state', $options);
            echo $this->Form->control('phone');
            echo $this->Form->control('url');
            echo $this->Form->control('email');
            echo $this->Form->control('bussiness_hours_start', ['empty' => true]);
            echo $this->Form->control('busssiness_hours_finish', ['empty' => true]);
              $options = [
    ['text' => '-Please Choose-', 'value' => '-Please Choose-', 'attr_name' => '-Please Choose-'],
    ['text' => 'Open', 'value' => 'Open', 'attr_name' => 'Open'],
    ['text' => 'Close', 'value' => 'Close', 'attr_name' => 'Close'],
   ];
echo $this->Form->select('public_holiday', $options);
echo $this->Form->control('shops._ids', ['options' => $shops]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Mall[]|\Cake\Collection\CollectionInterface $malls
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Mall'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Shops'), ['controller' => 'Shops', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Shop'), ['controller' => 'Shops', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="malls index large-9 medium-8 columns content">
    <h3><?= __('Malls') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                
                
                <th scope="col"><?= $this->Paginator->sort('Malls Name') ?></th>
               
                <th scope="col"><?= $this->Paginator->sort('state') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Contact') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Website') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($malls as $mall): ?>
            <tr>
                
               
                <td><?= h($mall->name) ?></td>
                
                <td><?= h($mall->state) ?></td>
                <td><?= h($mall->phone) ?></td>
                <td><?= h($mall->url) ?></td>
                
               
                
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $mall->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $mall->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $mall->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mall->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

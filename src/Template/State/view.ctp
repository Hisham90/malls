<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\State $state
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit State'), ['action' => 'edit', $state->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete State'), ['action' => 'delete', $state->id], ['confirm' => __('Are you sure you want to delete # {0}?', $state->id)]) ?> </li>
        <li><?= $this->Html->link(__('List State'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New State'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Malls'), ['controller' => 'Malls', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Mall'), ['controller' => 'Malls', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="state view large-9 medium-8 columns content">
    <h3><?= h($state->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($state->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($state->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($state->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($state->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Malls') ?></h4>
        <?php if (!empty($state->malls)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('About') ?></th>
                <th scope="col"><?= __('Address') ?></th>
                <th scope="col"><?= __('State') ?></th>
                <th scope="col"><?= __('Phone') ?></th>
                <th scope="col"><?= __('Url') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Bussiness Hours Start') ?></th>
                <th scope="col"><?= __('Busssiness Hours Finish') ?></th>
                <th scope="col"><?= __('Public Holiday') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($state->malls as $malls): ?>
            <tr>
                <td><?= h($malls->id) ?></td>
                <td><?= h($malls->user_id) ?></td>
                <td><?= h($malls->name) ?></td>
                <td><?= h($malls->about) ?></td>
                <td><?= h($malls->address) ?></td>
                <td><?= h($malls->state) ?></td>
                <td><?= h($malls->phone) ?></td>
                <td><?= h($malls->url) ?></td>
                <td><?= h($malls->email) ?></td>
                <td><?= h($malls->bussiness_hours_start) ?></td>
                <td><?= h($malls->busssiness_hours_finish) ?></td>
                <td><?= h($malls->public_holiday) ?></td>
                <td><?= h($malls->created) ?></td>
                <td><?= h($malls->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Malls', 'action' => 'view', $malls->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Malls', 'action' => 'edit', $malls->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Malls', 'action' => 'delete', $malls->id], ['confirm' => __('Are you sure you want to delete # {0}?', $malls->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>

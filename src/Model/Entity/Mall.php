<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Mall Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $about
 * @property string $address
 * @property string $state
 * @property string $phone
 * @property string $url
 * @property string $email
 * @property \Cake\I18n\FrozenTime $bussiness_hours_start
 * @property \Cake\I18n\FrozenTime $busssiness_hours_finish
 * @property string $public_holiday
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Shop[] $shops
 */
class Mall extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'name' => true,
        'about' => true,
        'address' => true,
        'state' => true,
        'phone' => true,
        'url' => true,
        'email' => true,
        'bussiness_hours_start' => true,
        'busssiness_hours_finish' => true,
        'public_holiday' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'shops' => true
    ];
}

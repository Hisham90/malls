<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Malls Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\ShopsTable|\Cake\ORM\Association\BelongsToMany $Shops
 *
 * @method \App\Model\Entity\Mall get($primaryKey, $options = [])
 * @method \App\Model\Entity\Mall newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Mall[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Mall|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Mall patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Mall[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Mall findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MallsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('malls');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Shops', [
            'foreignKey' => 'mall_id',
            'targetForeignKey' => 'shop_id',
            'joinTable' => 'malls_shops'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->allowEmpty('name');

        $validator
            ->scalar('about')
            ->allowEmpty('about');

        $validator
            ->scalar('address')
            ->allowEmpty('address');

        $validator
            ->scalar('state')
            ->allowEmpty('state');

        $validator
            ->scalar('phone')
            ->allowEmpty('phone');

        $validator
            ->scalar('url')
            ->allowEmpty('url');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->time('bussiness_hours_start')
            ->allowEmpty('bussiness_hours_start');

        $validator
            ->time('busssiness_hours_finish')
            ->allowEmpty('busssiness_hours_finish');

        $validator
            ->scalar('public_holiday')
            ->allowEmpty('public_holiday');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}

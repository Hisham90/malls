<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * State Controller
 *
 * @property \App\Model\Table\StateTable $State
 *
 * @method \App\Model\Entity\State[] paginate($object = null, array $settings = [])
 */
class StateController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $state = $this->paginate($this->State);

        $this->set(compact('state'));
        $this->set('_serialize', ['state']);
    }

    /**
     * View method
     *
     * @param string|null $id State id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $state = $this->State->get($id, [
            'contain' => ['Malls']
        ]);

        $this->set('state', $state);
        $this->set('_serialize', ['state']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $state = $this->State->newEntity();
        if ($this->request->is('post')) {
            $state = $this->State->patchEntity($state, $this->request->getData());
            if ($this->State->save($state)) {
                $this->Flash->success(__('The state has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The state could not be saved. Please, try again.'));
        }
        $malls = $this->State->Malls->find('list', ['limit' => 200]);
        $this->set(compact('state', 'malls'));
        $this->set('_serialize', ['state']);
    }

    /**
     * Edit method
     *
     * @param string|null $id State id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $state = $this->State->get($id, [
            'contain' => ['Malls']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $state = $this->State->patchEntity($state, $this->request->getData());
            if ($this->State->save($state)) {
                $this->Flash->success(__('The state has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The state could not be saved. Please, try again.'));
        }
        $malls = $this->State->Malls->find('list', ['limit' => 200]);
        $this->set(compact('state', 'malls'));
        $this->set('_serialize', ['state']);
    }

    /**
     * Delete method
     *
     * @param string|null $id State id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $state = $this->State->get($id);
        if ($this->State->delete($state)) {
            $this->Flash->success(__('The state has been deleted.'));
        } else {
            $this->Flash->error(__('The state could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

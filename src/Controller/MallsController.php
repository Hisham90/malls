<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Malls Controller
 *
 * @property \App\Model\Table\MallsTable $Malls
 *
 * @method \App\Model\Entity\Mall[] paginate($object = null, array $settings = [])
 */
class MallsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $malls = $this->paginate($this->Malls);

        $this->set(compact('malls'));
        $this->set('_serialize', ['malls']);
    }

    /**
     * View method
     *
     * @param string|null $id Mall id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $mall = $this->Malls->get($id, [
            'contain' => ['Users', 'Shops']
        ]);

        $this->set('mall', $mall);
        $this->set('_serialize', ['mall']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $mall = $this->Malls->newEntity();
        if ($this->request->is('post')) {
            $mall = $this->Malls->patchEntity($mall, $this->request->getData());
            if ($this->Malls->save($mall)) {
                $this->Flash->success(__('The mall has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The mall could not be saved. Please, try again.'));
        }
        $users = $this->Malls->Users->find('list', ['limit' => 200]);
        $shops = $this->Malls->Shops->find('list', ['limit' => 200]);
        $this->set(compact('mall', 'users', 'shops'));
        $this->set('_serialize', ['mall']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Mall id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $mall = $this->Malls->get($id, [
            'contain' => ['Shops']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $mall = $this->Malls->patchEntity($mall, $this->request->getData());
            if ($this->Malls->save($mall)) {
                $this->Flash->success(__('The mall has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The mall could not be saved. Please, try again.'));
        }
        $users = $this->Malls->Users->find('list', ['limit' => 200]);
        $shops = $this->Malls->Shops->find('list', ['limit' => 200]);
        $this->set(compact('mall', 'users', 'shops'));
        $this->set('_serialize', ['mall']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Mall id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $mall = $this->Malls->get($id);
        if ($this->Malls->delete($mall)) {
            $this->Flash->success(__('The mall has been deleted.'));
        } else {
            $this->Flash->error(__('The mall could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
